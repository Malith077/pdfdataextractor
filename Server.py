from flask import Flask, request, render_template, jsonify, url_for, make_response
import PyPDF2
import re
import os

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')

@app.route('/', methods=["POST"])
def upload_file():
    uploaded_file = request.files['file']
    address = request.form['address']
    if address == "":
        return make_response(jsonify(data = "invalid address"))
    if uploaded_file.filename != '':
        uploaded_file.save(uploaded_file.filename)
        results = readPDF(uploaded_file.filename, address)
        final = make_response(jsonify(
            data=results,
            status=200
            ))
        final.mimetype = "application/json"
        uploaded_file.close()
        
        return final
    return make_response(jsonify(data="No Data" ))


def readPDF(file, address):
    obj = open(file, 'rb')
    pdfObj = PyPDF2.PdfFileReader(obj)
    result = ''
    for i in range(pdfObj.numPages):
        result += pdfObj.getPage(i).extract_text()
    matches = textMatch(address, result)
    obj.close()
    os.remove(file)
    return str(matches * 100) + '%'


def textMatch(address, pdfFile):
    splitted_address = re.split(' |,|/', address.lower())
    splitted_address = list(filter(None, splitted_address))
    final_list = [False for _ in range(len(splitted_address))]
    for idx, x in enumerate(splitted_address):
        r = pdfFile.lower().find(x)
        if r > 0:
            final_list[idx] = True
    return final_list.count(True) / len(final_list)